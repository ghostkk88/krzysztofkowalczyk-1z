﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

	public bool startGame = true;
	public bool isLive = true;
	private Material material;
	public float ballScaleSpeed = 2.0f;
	private Color colourStart;
	private Color colourEnd;
	private float timerColor;

	public int ballLimit = 4;
	public int currentBallCount = 0;
	public float timeToShootBall = 1.0f;

	[Range(4, 10)]
	public float countdownValue = 10;
	private float currCountdownValue;

	void Start () 
	{
		colourEnd = new Color(Random.value, Random.value, Random.value);
		colourStart = colourEnd ;
		colourEnd.a = 0 ;

		material = GetComponent<Renderer>().material;
		if (startGame)
			StartCoroutine (NewBall());
		
		currentBallCount = 0;
	}

	void BallAnimation()
	{
		if (!isLive)
			return;

		transform.localScale = new Vector3(Mathf.PingPong((Time.time * ballScaleSpeed), 1), Mathf.PingPong((Time.time * ballScaleSpeed), 1), transform.position.z);

		timerColor += Time.deltaTime * ballScaleSpeed;
		material.color = Color.Lerp (colourStart, colourEnd, Mathf.PingPong(timerColor * 2, 1) );

		if(timerColor >= 1) 
		{
			timerColor = 0;
			colourEnd = new Color(Random.value, Random.value, Random.value);
			colourStart = colourEnd ;
			colourEnd.a = 0 ;
		}

	}
	IEnumerator NewBall ()
	{
		while ((currentBallCount <= ballLimit) && (GameController.stacicInstance.allCurrentBallCount <= GameController.stacicInstance.ballLimit) && (GameController.stacicInstance.activeBallCount >= 0))
		{
			yield return new WaitForSeconds (timeToShootBall);

			if (GameController.stacicInstance.allCurrentBallCount < GameController.stacicInstance.ballLimit)
			{
				transform.GetComponent<Rigidbody> ().isKinematic = true;
				GameObject newObject;
				newObject = Instantiate (gameObject, transform.position, Quaternion.identity);
				newObject.transform.GetComponent<SphereCollider> ().enabled = false;
				newObject.transform.GetComponent<Rigidbody> ().isKinematic = false;
				newObject.transform.GetComponent<Rigidbody> ().AddForce (Random.Range (-20, 20), Random.Range (-20, 20), 0);
				newObject.transform.GetComponent<BallController> ().startGame = false;
				newObject.transform.GetComponent<BallController> ().StartCoroutine ("StartCountdown");

				currentBallCount++;
				GameController.stacicInstance.allCurrentBallCount++;
				GameController.stacicInstance.ballList.Add (newObject);

				if (currentBallCount == ballLimit) 
				{
					DeadAction ();
					yield break;
				}
			}
		}
	}
	public IEnumerator StartCountdown()
	{
		currCountdownValue = countdownValue;
		while (currCountdownValue >= 0)
		{
			if (!transform.GetComponent<SphereCollider> ().enabled) 
			{
				yield return new WaitForSeconds (0.7f);
				transform.GetComponent<SphereCollider> ().enabled = true;
			}
			
			yield return new WaitForSeconds(1.0f);
			//transform.GetComponent<SphereCollider> ().enabled = true;
			currCountdownValue--;
			if (currCountdownValue <= 0)
			{
				transform.GetComponent<Rigidbody> ().isKinematic = true;
				StartCoroutine (NewBall());
				yield break;
			}
		}
	}
	void OnCollisionEnter(Collision col)
	{
		if (col.transform.tag == "Ball") 
		{
			col.transform.gameObject.SetActive (false);
			gameObject.SetActive (true);
			if (gameObject.activeInHierarchy)
				DeadAction ();
			StopCoroutine(NewBall());
			StopCoroutine(StartCountdown());
		}
	}
	void DeadAction()
	{
		isLive = false;
		material.color = Color.red;
		transform.GetComponent<Rigidbody> ().isKinematic = true;
		transform.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		GameController.stacicInstance.deadBallCount++;
	}

	void Update () 
	{
		BallAnimation ();
	}
}
