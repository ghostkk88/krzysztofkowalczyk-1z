﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public int ballLimit = 200;
	public int allCurrentBallCount = 0;
	public int activeBallCount = 1;
	public int deadBallCount = 0;
	public Text allBallsText;
	public Text activeBallsText;
	public Text deadBallsText;
	//public Transform ball;
	public List<GameObject> ballList = new List<GameObject>(); 

	public static GameController stacicInstance = null;

	void Awake()
	{
		if (stacicInstance == null)
			stacicInstance = this;
		else if (stacicInstance != null)
			Debug.Log ("(stacicInstance != null)");
	}
		
		
	// Update is called once per frame
	void Update () 
	{
		activeBallCount = allCurrentBallCount - deadBallCount;

		if (activeBallCount < 0)
			activeBallCount = 0;
		
		allBallsText.text = "All balls: " + allCurrentBallCount.ToString ();
		activeBallsText.text = "Active balls: " + activeBallCount.ToString ();
		deadBallsText.text = "Dead balls: " + deadBallCount.ToString ();

		if ((allCurrentBallCount >= ballLimit) || (activeBallCount <= 0))
		{
			for (int i = 0; i < ballList.Count; i++) 
			{
				ballList [i].transform.GetComponent<BallController> ().StopCoroutine("NewBall");
				ballList [i].transform.GetComponent<BallController> ().StopCoroutine("StartCountdown");
				ballList [i].transform.GetComponent<SphereCollider> ().enabled = true;
			}
		}			
	}
}
